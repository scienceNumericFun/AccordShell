namespace AccordShell.Statistics.Regression

open Accord.Math.Optimization
open System
open Microsoft.FSharp.Core
open System.Collections
open Microsoft.FSharp.Collections

module GeneralSolver =
    type MeasurementPoint =
        {   xVec: double list;
            yVal: double }
    type RegressionProblem() =
        member val XMeas : double list list = [[0.]] with get,set
        member val YMeas : double list = [0.] with get,set
        member val Parameter : double list = [0.] with get,set
        member val ProblemFormula : double list -> double list -> double = fun (p)(x) -> p.[0] + p.[1] * x.[0] with get,set
    let invokeProblemFormula (  regProb:RegressionProblem,
                                xVal:double[]
                             ) =
        let xValList =  xVal
                        |> List.ofSeq
        let y = regProb.ProblemFormula regProb.Parameter xValList
        y
    let createProbByMeasAndFormula (xMeasIn:double[][],
                                    yMeasIn:double[],
                                    formula:System.Func<System.Collections.Generic.List<double>,System.Collections.Generic.List<double>,double>,
                                    noOfParam:int
                                    ) =
        let fcnFS = fun (x:double list) (y: double list) -> formula.Invoke(Generic.List<double>(x),Generic.List<double>(y))
        let xMeasInList =   List.ofSeq xMeasIn
                                |> List.map List.ofSeq
        let yMeasInList = List.ofSeq yMeasIn
        let initParam = Array.zeroCreate<double> noOfParam
                            |> List.ofArray
        let regProb = RegressionProblem()
        regProb.XMeas <- xMeasInList
        regProb.YMeas <- yMeasInList
        regProb.Parameter <- initParam
        regProb.ProblemFormula <- fcnFS
        regProb
        //{xMeas = xMeasInList; yMeas = yMeasInList; problemFormula = fcnFS; parameter = initParam }
    let solveRegressionProblem (regProb:RegressionProblem) =
        let measurementSet =
            (regProb.XMeas,regProb.YMeas)
            ||> List.map2 (fun x y -> {xVec = x; yVal = y})
        let yDifference (para:double list)(xy:MeasurementPoint) =
            regProb.ProblemFormula para xy.xVec - xy.yVal
        let yLeastSquareErr (para:double list)(xyList:MeasurementPoint list) =
            xyList
            |> List.map (fun x -> yDifference para x)
            |> List.map (fun x -> x * x)
            |> List.sum
        // careful! we need double arrays
        let optimizationProblem (para:double[]) =
            let paraList =
                Array.toList para
            yLeastSquareErr paraList measurementSet
        let nelderMeadSolver =
            NelderMead(regProb.Parameter.Length)
        nelderMeadSolver.Function <-
            Func<float[],float> optimizationProblem
        nelderMeadSolver.Minimize()
            |> ignore
        regProb.Parameter <-
            Array.toList nelderMeadSolver.Solution
        ()
    let solveRegressionProblemWithMethod (regProb:RegressionProblem)(optMethod:BaseOptimizationMethod ) =
        let measurementSet =
            (regProb.XMeas,regProb.YMeas)
            ||> List.map2 (fun x y -> {xVec = x; yVal = y})
        let yDifference (para:double list)(xy:MeasurementPoint) =
            regProb.ProblemFormula para xy.xVec - xy.yVal
        let yLeastSquareErr (para:double list)(xyList:MeasurementPoint list) =
            xyList
            |> List.map (fun x -> yDifference para x)
            |> List.map (fun x -> x * x)
            |> List.sum
        // careful! we need double arrays
        let optimizationProblem (para:double[]) =
            let paraList =
                Array.toList para
            yLeastSquareErr paraList measurementSet
        optMethod.Function <-
            Func<float[],float> optimizationProblem
        optMethod.NumberOfVariables <-
            regProb.Parameter.Length
        optMethod.Minimize()
            |> ignore
        regProb.Parameter <-
            Array.toList optMethod.Solution
        ()
