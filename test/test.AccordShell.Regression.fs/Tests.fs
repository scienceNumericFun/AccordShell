module Tests
open System
open Xunit
open AccordShell.Statistics.Regression.GeneralSolver
open System.Collections

let getRegressionObj () =
    let xMeasTest = [[double 1.];[double 2.];[double 3.];[double 4.]]
                        |> List.map List.toArray
                        |> List.toArray
    let yMeasTest = [double 3.;double 5.;double 7.;double 9.]
                        |> List.toArray
    let formula (para:Generic.List<double>)(xMeas:Generic.List<double>) =
        para.[0] + para.[1] * xMeas.[0]
    let formulaFcn =  System.Func<Generic.List<double>,Generic.List<double>,double> formula
    createProbByMeasAndFormula(xMeasTest, yMeasTest, formulaFcn, 2)

[<Fact>]
let ``Test Regression Problem Build`` () =
    let regObj = getRegressionObj ()
    Assert.NotNull(regObj)
    ()
[<Fact>]
let ``Test Regression Solver Gradient Free`` () =
    let regObj = getRegressionObj ()
    solveRegressionProblem regObj
    let mutable lowerParameterBound = double 1.
    let mutable upperParameterBound = double 3.
    Assert.InRange(regObj.Parameter.[1],lowerParameterBound,upperParameterBound )
    lowerParameterBound <- 0.
    upperParameterBound <- 2.0
    Assert.InRange(regObj.Parameter.[0],lowerParameterBound,upperParameterBound )
    ()
[<Fact>]
let ``Test Regression Solver with Method Object`` () =
    let regObj = getRegressionObj ()
    let method = Accord.Math.Optimization.Cobyla(2)
    solveRegressionProblemWithMethod regObj method
    let mutable lowerParameterBound = double 1.
    let mutable upperParameterBound = double 3.
    Assert.InRange(regObj.Parameter.[1],lowerParameterBound,upperParameterBound )
    lowerParameterBound <- 0.
    upperParameterBound <- 2.
    Assert.InRange(regObj.Parameter.[0],lowerParameterBound,upperParameterBound )
    ()
[<Fact>]
let ``Test Invoke Problem Formula `` () =
    let regObj = getRegressionObj ()
    solveRegressionProblem regObj
    let xForY = [1.;2.;3.;4.]
    let yComput =   xForY
                    |> List.map (fun x -> [| double x|])
                    |> List.map (fun x -> invokeProblemFormula(regObj,x))
    let yTrue = [3.;5.;7.;9.]
                |> List.map (fun x -> double x)
    (yTrue,yComput) 
    ||> List.map2 (fun x y -> Assert.InRange(y,x-1.,x+1.))
    |> ignore
    ()
